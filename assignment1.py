import random
def dicerollFunction(diceMaxValue):
    dicerollResult=random.randint(1, diceMaxValue)
    return dicerollResult


def main():
    diceMaxValue=6
    dicerollStatus=True
    while dicerollStatus:
        userChoice=input("Ready to roll? Enter Q to Quit")
        if userChoice.lower() !="q":
            dicerollResult=dicerollFunction(diceMaxValue)
            print("You have rolled a",dicerollResult)
        else:
            dicerollStatus=False